<?php
/**
 * Copyright © Aspire Systems, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Aspire\Hotlinks\Block\Adminhtml\Item\Edit;

use Magento\Backend\Block\Widget\Context;

abstract class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     */
    public function __construct(Context $context)
    {
        $this->context = $context;
    }

    /**
     * Return model ID
     *
     * @return int|null
     */
    public function getModelId()
    {
        return $this->context->getRequest()->getParam('item_id');
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string  $route
     * @param   array   $params
     *
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
