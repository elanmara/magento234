<?php
/**
 * Copyright © Aspire Systems, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Aspire\Hotlinks\Block\Adminhtml\Page\Edit;

class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry             $registry
     * @param \Magento\Framework\Data\FormFactory     $formFactory
     * @param array                                   $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Cms\Model\Wysiwyg\Config $wysiwygConfig,
        array $data = []
    ) {
        $this->_wysiwygConfig = $wysiwygConfig;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Prepare form.
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $dateFormat = $this->_localeDate->getDateFormat(\IntlDateFormatter::SHORT);
        $model = $this->_coreRegistry->registry('row_data');
        $form = $this->_formFactory->create(
            ['data' => [
                'id' => 'edit_form',
                'enctype' => 'multipart/form-data',
                'action' => $this->getData('action'),
                'method' => 'post',
            ],
            ]
        );
        $form->setHtmlIdPrefix('post_');
        if ($model->getPagetId()) {

            $fieldset = $form->addFieldset(
                'base_fieldset',
                ['legend' => __('Edit'), 'class' => 'fieldset-wide']
            );
        } else {
            $fieldset = $form->addFieldset(
                'base_fieldset',
                ['legend' => __('Page'), 'class' => 'fieldset-wide']
            );
        }

        $fieldset->addField('page_id', 'hidden', ['name' => 'page_id']);

        $fieldset->addField(
            'page_title',
            'text',
            [
                'name' => 'page_title',
                'label' => __('Page Title'),
                'id' => 'page_title',
                'title' => __('Page Title'),
                'required' => true,
            ]
        );

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
