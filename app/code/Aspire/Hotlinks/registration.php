<?php
/**
 * Copyright © Aspire Systems, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Aspire_Hotlinks', __DIR__);
