<?php
/**
 * Copyright © Aspire Systems, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Aspire\Hotlinks\Api\Data;

interface ItemGridInterface
{
    /**
     * Constants for keys of data array.
     */
    const ITEM_ID = 'item_id';
    const TITLE = 'item_title';
    const DESCRIPTION = 'item_desc';
    const ALT = 'item_alt';
    const URL = 'item_url';
    const IMAGE = 'item_image';
    const PAGE_ID = 'page_id';
    const ENTITY_TYPE_ID = 'entity_type_id';
    const CREATED_AT = 'created_at';

}
