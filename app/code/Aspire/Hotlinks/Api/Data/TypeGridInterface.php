<?php
/**
 * Copyright © Aspire Systems, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Aspire\Hotlinks\Api\Data;

interface TypeGridInterface
{
    /**
     * Constants for keys of data array.
     */
    const TYPE_ID = 'entity_type_id';
    const TYPE = 'entity_type';
    const CREATED_AT = 'created_at';

    /**
     * Get EntityTypeId.
     *
     * @return int
     */
    public function getEntityTypeIdId();

    /**
     * Set EntityTypeId.
     */
    public function setEntityTypeId($entityTypeId);

    /**
     * Get EntityType.
     *
     * @return varchar
     */
    public function getEntityType();

    /**
     * Set EntityType.
     */
    public function setEntityType($type);

    /**
     * Get CreatedAt.
     *
     * @return varchar
     */
    public function getCreatedAt();

    /**
     * Set CreatedAt.
     */
    public function setCreatedAt($createdAt);
}
