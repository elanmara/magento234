<?php
/**
 * Copyright © Aspire Systems, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Aspire\Hotlinks\Api\Data;

interface PageGridInterface
{
    /**
     * Constants for keys of data array.
     */
    const PAGE_ID = 'page_id';
    const TITLE = 'page_title';
    const CREATED_AT = 'created_at';

    /**
     * Get PageId.
     *
     * @return int
     */
    public function getPageId();

    /**
     * Set PageId.
     */
    public function setPageId($pageId);

    /**
     * Get PageTitle.
     *
     * @return varchar
     */
    public function getPageTitle();

    /**
     * Set PageTitle.
     */
    public function setPageTitle($pageTitle);

    /**
     * Get CreatedAt.
     *
     * @return varchar
     */
    public function getCreatedAt();

    /**
     * Set CreatedAt.
     */
    public function setCreatedAt($createdAt);
}
