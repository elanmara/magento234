<?php
/**
 * Copyright © Aspire Systems, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Aspire\Hotlinks\Controller\Adminhtml\Form;

class Save extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Index';

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Aspire\Hotlinks\Model\PageGridFactory
     */
    protected $itemFactory;

    /**
     * @param \Magento\Backend\App\Action\Context                   $context
     * @param \Magento\Framework\View\Result\PageFactory            $resultPageFactory
     * @param \Aspire\Hotlinks\Model\ItemGridFactory                $itemFactory
     */

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Aspire\Hotlinks\Model\ItemGridFactory $itemFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->itemFactory = $itemFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        // echo "<pre>"; print_r($data); exit;
        if ($data['dynamic_rows_container']) {
            try {
                $item = $this->itemFactory->create();
                $dynamicData = $data['dynamic_rows_container'];
                foreach($dynamicData as $data){
                    $data = array_filter($data, function ($value) {
                        return $value !== '';
                    });
                    $item->setData($data);
                    $item->save();
                }
                $this->messageManager->addSuccess(__('Successfully saved the item.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                return $resultRedirect->setPath('*/item/index');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData($data);
                return $resultRedirect->setPath('*/*/edit', ['item_id' => $item->getItemId()]);
            }
        }
        return $resultRedirect->setPath('*/item/index');
    }
}
