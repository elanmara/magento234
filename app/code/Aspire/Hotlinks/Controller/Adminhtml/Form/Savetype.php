<?php
/**
 * Copyright © Aspire Systems, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Aspire\Hotlinks\Controller\Adminhtml\Form;

class Savetype extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Index';

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Aspire\Hotlinks\Model\TypeGridFactory
     */
    protected $typeFactory;

    /**
     * @param \Magento\Backend\App\Action\Context                   $context
     * @param \Magento\Framework\View\Result\PageFactory            $resultPageFactory
     * @param \Aspire\Hotlinks\Model\TypeGridFactory                $typeFactory
     */

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Aspire\Hotlinks\Model\TypeGridFactory $typeFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->typeFactory = $typeFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            try {
                $id = $data['entity_type_id'];
                $type = $this->typeFactory->create()->load($id);
                $data = array_filter($data, function ($value) {
                    return $value !== '';
                });
                $type->setData($data);
                $type->save();
                $this->messageManager->addSuccess(__('Successfully saved the type.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                return $resultRedirect->setPath('*/type/index');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData($data);
                return $resultRedirect->setPath('*/*/edit', ['entity_type_id' => $type->getEntityTypeIdId()]);
            }
        }
        return $resultRedirect->setPath('*/type/index');
    }
}
