<?php
/**
 * Copyright © Aspire Systems, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Aspire\Hotlinks\Controller\Adminhtml\Form;

class Savepage extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Index';

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Aspire\Hotlinks\Model\PageGridFactory
     */
    protected $pageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context                   $context
     * @param \Magento\Framework\View\Result\PageFactory            $resultPageFactory
     * @param \Aspire\Hotlinks\Model\PageGridFactory                $pageFactory
     */

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Aspire\Hotlinks\Model\PageGridFactory $pageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->pageFactory = $pageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            try {
                $id = $data['page_id'];
                $page = $this->pageFactory->create()->load($id);
                $data = array_filter($data, function ($value) {
                    return $value !== '';
                });
                $page->setData($data);
                $page->save();
                $this->messageManager->addSuccess(__('Successfully saved the page.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
                return $resultRedirect->setPath('*/page/index');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData($data);
                return $resultRedirect->setPath('*/*/edit', ['page_id' => $page->getPageId()]);
            }
        }
        return $resultRedirect->setPath('*/page/index');
    }
}
