<?php
/**
 * Copyright © Aspire Systems, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Aspire\Hotlinks\Model\Config\Source;

use Aspire\Hotlinks\Model\ResourceModel\TypeGrid\CollectionFactory;
use Magento\Framework\Option\ArrayInterface;

class Type implements ArrayInterface
{

    /**
     * @var CollectionFactory
     */
    protected $typeCollection;

    public function __construct(
        CollectionFactory $typeCollection
    ) {
        $this->typeCollection = $typeCollection;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $typeCollection = $this->typeCollection->create();
        $optionsArray = array();
        foreach ($typeCollection as $type) {
            // echo "<pre>"; print_r($type->getData()); exit;
            $optionsArray['label'] = $type->getData('entity_type');
            $optionsArray['value'] = $type->getData('entity_type_id');
        }
        $options[] = $optionsArray;

        return $options;
    }

}
