<?php
/**
 * Copyright © Aspire Systems, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Aspire\Hotlinks\Model\Config\Source;

use Aspire\Hotlinks\Model\ResourceModel\PageGrid\CollectionFactory;
use Magento\Framework\Option\ArrayInterface;

class Page implements ArrayInterface
{

    /**
     * @var CollectionFactory
     */
    protected $pageCollection;

    public function __construct(
        CollectionFactory $pageCollection
    ) {
        $this->pageCollection = $pageCollection;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $pageCollection = $this->pageCollection->create();
        $optionsArray = array();
        foreach ($pageCollection as $page) {
            $optionsArray['label'] = $page->getData('page_title');
            $optionsArray['value'] = $page->getData('page_id');
        }
        $options[] = $optionsArray;

        return $options;
    }

}
