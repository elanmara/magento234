<?php
/**
 * Copyright © Aspire Systems, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Aspire\Hotlinks\Model;

use Aspire\Hotlinks\Api\Data\ItemGridInterface;

class ItemGrid extends \Magento\Framework\Model\AbstractModel implements ItemGridInterface
{
    /**
     * CMS page cache tag.
     */
    const CACHE_TAG = 'aspire_item_grid_records';

    /**
     * @var string
     */
    protected $_cacheTag = 'aspire_item_grid_records';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'aspire_item_grid_records';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Aspire\Hotlinks\Model\ResourceModel\ItemGrid');
    }
}
