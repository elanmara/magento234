<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Aspire\Hotlinks\Model;

use Aspire\Hotlinks\Api\Data\TypeGridInterface;

class TypeGrid extends \Magento\Framework\Model\AbstractModel implements TypeGridInterface
{
    /**
     * CMS page cache tag.
     */
    const CACHE_TAG = 'aspire_type_grid_records';

    /**
     * @var string
     */
    protected $_cacheTag = 'aspire_type_grid_records';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'aspire_type_grid_records';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Aspire\Hotlinks\Model\ResourceModel\TypeGrid');
    }

    /**
     * Get PageId.
     *
     * @return int
     */
    public function getEntityTypeIdId()
    {
        return $this->getData(self::TYPE_ID);
    }

    /**
     * Set EntityId.
     */
    public function setEntityTypeId($entityTypeId)
    {
        return $this->setData(self::TYPE_ID, $entityTypeId);
    }

    /**
     * Get Title.
     *
     * @return varchar
     */
    public function getEntityType()
    {
        return $this->getData(self::TYPE);
    }

    /**
     * Set Type.
     */
    public function setEntityType($type)
    {
        return $this->setData(self::TYPE, $type);
    }

    /**
     * Get CreatedAt.
     *
     * @return varchar
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Set CreatedAt.
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }
}
