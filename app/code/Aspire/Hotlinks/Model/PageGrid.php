<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Aspire\Hotlinks\Model;

use Aspire\Hotlinks\Api\Data\PageGridInterface;

class PageGrid extends \Magento\Framework\Model\AbstractModel implements PageGridInterface
{
    /**
     * CMS page cache tag.
     */
    const CACHE_TAG = 'aspire_page_grid_records';

    /**
     * @var string
     */
    protected $_cacheTag = 'aspire_page_grid_records';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'aspire_page_grid_records';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Aspire\Hotlinks\Model\ResourceModel\PageGrid');
    }

    /**
     * Get PageId.
     *
     * @return int
     */
    public function getPageId()
    {
        return $this->getData(self::PAGE_ID);
    }

    /**
     * Set EntityId.
     */
    public function setPageId($pageId)
    {
        return $this->setData(self::PAGE_ID, $pageId);
    }

    /**
     * Get Title.
     *
     * @return varchar
     */
    public function getPageTitle()
    {
        return $this->getData(self::TITLE);
    }

    /**
     * Set Title.
     */
    public function setPageTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Get CreatedAt.
     *
     * @return varchar
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Set CreatedAt.
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }
}
