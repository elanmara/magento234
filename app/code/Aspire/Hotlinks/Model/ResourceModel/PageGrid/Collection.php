<?php
/**
 * Copyright © Aspire Systems, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Aspire\Hotlinks\Model\ResourceModel\PageGrid;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'page_id';
    /**
     * Define resource model.
     */
    protected function _construct()
    {
        $this->_init('Aspire\Hotlinks\Model\PageGrid', 'Aspire\Hotlinks\Model\ResourceModel\PageGrid');
    }
}
