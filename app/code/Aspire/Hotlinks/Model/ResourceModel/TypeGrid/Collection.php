<?php
/**
 * Copyright © Aspire Systems, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Aspire\Hotlinks\Model\ResourceModel\TypeGrid;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'entity_type_id';
    /**
     * Define resource model.
     */
    protected function _construct()
    {
        $this->_init('Aspire\Hotlinks\Model\TypeGrid', 'Aspire\Hotlinks\Model\ResourceModel\TypeGrid');
    }
}
