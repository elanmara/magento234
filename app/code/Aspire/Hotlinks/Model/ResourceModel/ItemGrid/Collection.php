<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Aspire\Hotlinks\Model\ResourceModel\ItemGrid;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'item_id';
    /**
     * Define resource model.
     */
    protected function _construct()
    {
        $this->_init('Aspire\Hotlinks\Model\ItemGrid', 'Aspire\Hotlinks\Model\ResourceModel\ItemGrid');
    }
}
